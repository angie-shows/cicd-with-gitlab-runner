# CI/CD with Gitlab Runner 🛠️ (with examples and results!)

> I am writing this to document what i've learnt about CI/CD the past 70 over commits before i forget.

At the time of writing, i only understand enough to get (very) basic things working.

If you're like me, overwhelmed by sheer amount of information and don't know where to begin, i hope this documentation can at least help you get started with a clearer sense/concept of Gitlab's CI/CD by getting a few very simple samples to work.


### Gitlab Runner 👨‍💻 and `.gitlab-ci.yml` 📋

Gitlab Runner, as the name suggests, it runs. But scripts and commands, not marathons.

**This is how i imagine the Gitlab Runner operates: 🤔**

> There is a *house* in the clouds that your codes live in (your **repo**).
>
> The **runner** is like a *butler* that lives in the house.
>
> The butler works (**runs scripts**) on *its own computer* in exchange for "living in the house for free".
>
> The butler works according to a *contract* (**`.gitlab-ci.yml`** file).
>
> Whenever there's a package (pushed commit) arrived at the house, the butler will look at the contract, and check if there's anything needed to be done.

**📖 Please read Gitlab's documentation for a more accurate explanation.**

Without CI/CD, the workflow for a static site update could be you, sitting in front of your computer and do these:

1. Make and commit changes
2. Compile static files
3. Upload static files to staging (delivery)
4. Upload same files to production (deployment)

> For this example's context, let's just assume you are running scripts to all the above.

Delegating the deployment workflow to a Gitlab Runner will change

- FROM: **you** sitting in front of **your local machine** running the scrips
- TO: **Gitlab Runner** sitting in your repo, running the scripts on "its own computer"

Let's draft the contract.


### Writing `.gitlab-ci.yml` 📝

How Gitlab Runner runs your `.gitlab-ci.yml`:

The runner turns on its computer and **spins up a "virtual machine"** and in that virtual machine, it **pulls up a terminal and runs the scripts** you specified.


#### Step 1: Creating a job

So in your `.gitlab-ci.yml`, you need to specify at least **one job** with **one job keyword**.

  > You can give your job with **any name** you want, as long as it's not one of the [unavailable names](https://docs.gitlab.com/ee/ci/yaml/#unavailable-names-for-jobs).
  >
  > To see the full list of **job keywords** [here](https://docs.gitlab.com/ee/ci/yaml/#job-keywords).


So you only need 3 crucial lines in `.gitlab-ci.yml` to trigger a job run:

```yml
# .gitlab-ci.yml

My cool job: # job name
  script: # job keyword
    # scripts to run
    - echo "Hello there! Running the only job in .gitlab-ci.yml file!"
```

Commit and push `.gitlab-ci.yml`.

Now we can view the running pipeline.

Go to your repo and on the side panel, click **CI / CD** > **Pipelines**, and you'll see:

![01](assets/01.jpg)

Click the first pipeline (status) listed. It should say `pending` or `running`.

Pending simply means that the pipeline was created, and it will be run when it's triggered.

![02](assets/02.png)

The `Test` you see above the job name is a pipeline name.

**Jobs** will always be grouped into a **pipeline**.

When you don't specify a pipeline, a default pipeline `test` will be created for you, and that's where your jobs will be executed.

Then click on the job name and you'll see the **"runner's terminal"**!

![03](assets/03.jpg)

In top red box, you can see that the runner is using **Docker** to pull a disk image `ruby:2.5`.

And in the bottom red box, you can see that our script is being executed!

Gitlab Runner will always pull an image (using Docker) to create a **container**. **Each job** will be executed in a **seperate container**. (One job, one container.)

Default image is `ruby:2.5` when you don't specify which image to use.


#### Step 2: Creating pipelines

Let's specify some pipelines to override the default.

```yml
# .gitlab-ci.yml

# define Pipeline(s) with using keyword "stages"
stages:
  - greet # pipeline name

# your job name
My cool job:
  stage: greet # specify which in which stage this job will run
  script:
    - echo "Hello there!"
```

And then we stalk our pipelines:

![04](assets/04.jpg)

Watch it turns from `pending`...

![05](assets/05.jpg)

To `running`...and click to view the job again!

![06](assets/06.jpg)

Great! Now the pipeline name is showing what we specified.

Let's try adding more pipelines and jobs.

```yml
# .gitlab-ci.yml

# Define Pipelines
stages:
  - greet me
  - chat with me
# Pipeline gets triggered according to the ORDER as listed above.
# So "greet me" will run first, and when there's no error,
# "chat with me" will run

# Define Jobs
Say Hello:
  stage: greet me
  script:
    - echo "Hello there!"

Who am i:
  stage: chat with me
  script:
    - echo "I am Gitlab Runner."

Day of week:
  stage: chat with me
  script:
    - DOW=$(date +%A)
    - echo "Today is $DOW."
# If there are multiple jobs under the SAME pipeline,
# they will be triggered CONCURRENTLY.
```

After we push, we'll see:

![07](assets/07.jpg)

**TWO circles** instead of one! (One for each stage we defined.)

![08](assets/08.jpg)

`chat with me` pipeline pending to start...

![09](assets/09.jpg)

Job `Day of week` and `Who am i` are running concurrently after pipeline `greet me` is finished without error!

![10](assets/10.jpg)


#### Step 3: Global Keywords

[Global keywords](https://docs.gitlab.com/ee/ci/yaml/#global-defaults) are the keywords that will be used in **all jobs**. They can be overriden by job-specific keywords.

```yml
# .gitlab-ci.yml

# image that will be used to build
# a fresh CONTAINER for EVERY JOB listed below
image: alpine:latest

# "before_script" are simply the scripts to run
# before our main "script" runs
before_script:
  - echo "Hi!"

stages:
  - chat with me
  - show location

Who am i:
  # "Who am i" will be run in a container built with
  # image "alpine:latest", as specified in global keyword
  stage: chat with me
  script:
    - echo "I am Gitlab Runner."

Where am i:
  image: ruby:2.4
  # because an image is specified in this job,
  # "Where am i" will be run in a container that is built with "ruby:2.4"
  stage: show location
  script:
    - echo "Current working directory is"
    - pwd
```

And if you open up both jobs:

![11](assets/11.jpg)

You will see `echo "Hi!"` being executed before each script in both jobs!

![12](assets/12.jpg)

"Who am i" job executed in a container built using `alpine:latest`

![13](assets/13.jpg)

"Where am i" job executed in a container built using `ruby:2.4`


#### Step 4: CI / CD Variables

You can define **variables** that can be used in your `.gitlab-ci.yml`.

Go to **Settings > CI / CD**, scroll to **Variables**, click **Expand**, and **Add Variable**.

![14](assets/14.jpg)

And fill in your key value pair. Key with *variable name*, Value with *variable value*. You can create string **variables** or **file**.

Let's create 3 variables, 2 variables and 1 file.

`MY_SPECIAL_ENV_VAR=acoustic`

![15](assets/15.jpg)

`SUPER_SECRET_VAR=somethingsensitive`

![16](assets/16.jpg)

If you check the **Mask variable** box, `somethingsensitive` will be masked in job terminals (replaced with `[MASKED]`).

Here's another variable `HIDE_ROOT_USER` that will mask anything that matched `root-user`.

![18](assets/18.jpg)

`INDEX_HTML_FILE` = html file content

![17](assets/17.jpg)

Select "File" as type.


```yml
# .gitlab-ci.yml
image: alpine:latest

Show me my vars:
  script:
    - echo "My favorite genre is $MY_SPECIAL_ENV_VAR."
    - echo "My password is $SUPER_SECRET_VAR."
    - echo "Runner is $(whoami)-user in container."
    - cat $INDEX_HTML_FILE
```

![19](assets/19.jpg)

As you can see, **masked** variables' values will not be shown.

Depending on your repo settings, if your pipeline settings are public, job logs will be public as well.

So keep that in mind and be careful when setting up variables.

Settings can be made in **Settings > CI / CD**, click **Expand**, and uncheck **Public pipelines**.

![20](assets/20.jpg)


#### Step 5: Branch-based jobs

You can specify a job to only be executed on a **certain branch**.

**Example 1: jobs only**
```yml
# .gitlab-ci.yml
image: alpine:latest

Day of week:
  script:
    - echo "Today is $(date +%A)."

Show location:
  only:
  # using keyword "only" to limit
  # "show location" job to run on MASTER branch only
    - master
  script:
    - echo "Master branch working directory is $(pwd)."
```

If you push the above file to both `master` and `non-master` branches, and look at the jobs for each branch:

![21](assets/21.jpg)

Issue branch

![22](assets/22.jpg)

Master branch

**Example 2: jobs with stages**
```yml
# .gitlab-ci.yml
image: alpine:latest

stages:
  - lint
  - deploy

Lint all files:
  stage: lint
  script:
    - echo "Insert cool linting scripts here."

Upload files:
  stage: deploy
  only:
    - master # "upload files will only run on master branch"
  script:
    - echo "Up up and away 🚀"
```

You will get the following:

![23](assets/23.jpg)

On **Issue** branch: Only "Lint all files" job was executed in issue branch.

![24](assets/24.jpg)

On **Master** branch: Both "Lint all files" and "Upload files" jobs were executed.

And "Lint all files" will run before "Upload files" like stated in the `stages` order.


#### Step 6: SSH Automation 🔒

Now let's learn how to automate SSH logins and solve the last piece of the puzzle.

You can choose to authenticate SSH logins **public/private key (recommended)** or **password (less secure)**.

This documentation will cover both approaches to SSH in. So depending on your remote server configurations and security level, choose one that suits your needs/preferences.

We'll start by creating some **variables and/or files** we'll be using.

1. **File** - `JOHN_PRIVATE_KEY` *🚩 [Skip this for password auth]*

  ![25](assets/25.jpg)

  > ⚠️ Server setup: Make sure you've added the user's **public key** into `.ssh/authorized_keys`!
  >
  > ⚠️ Copy the **contents** of the remote user's **private key** file.
  >
  > It's vital that you keep the empty line at the end of the file content to avoid `load pubkey "/root/.ssh/id_rsa": invalid format` error.
  >
  > ![25-b](assets/25-b.jpg)

2. **Masked Variable** - `JOHN_PASSWORD` *🚩 [Skip this for private key auth]*

  ![26](assets/26.jpg)

3. **Masked Variables** - `DOMAIN` and `REMOTE_IP` *[Skip this if you don't mind these info being exposed in the logs.]*

  ![27](assets/27.jpg)


**SSH with private key**

```yml
image: alpine:latest

SSH with private key:
  before_script:
    - apk add openssh-client
    # Install openssh-client package to the CONTAINER.
    # "apk add package1 package2" is how you add / install packages in alpine.

    - mkdir ~/.ssh && chmod 700 ~/.ssh
    # Create SSH folder in the CONTAINER and give it the correct permission.

    - cp $JOHN_PRIVATE_KEY ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
    # Use the private key as the CONTAINER's private key & update permission

    - ssh-keyscan $DOMAIN >> ~/.ssh/known_hosts
    # Use "ssh-keyscan" to retrieve the public SSH host keys of your domain
    # and store it in the CONTAINER's "known_hosts"

  script:
    - echo "Runner is in $(pwd) as $(whoami)."
    # From this "pwd", you will see the working directory
    # is still in the CONTAINER (/builds/username/repo-name)

    - ssh johnsmith@$DOMAIN "pwd && whoami && echo \"SSH in successfully as \$(whoami) with private key authentication!\""
    # The "pwd" and "whoami" in this line is executed in the remote server.

    - echo "Runner is back in $(pwd) as $(whoami)."
    # And on this command, the Runner is back to the CONTAINER.
```

![28](assets/28.jpg)


**SSH with password**

```yml
image: alpine:latest

SSH with password:
  before_script:
    - apk add openssh-client sshpass
    # Install openssh-client and sshpass packages to the CONTAINER.
    # "apk add package1 package2" is how you add / install packages in alpine.

    - mkdir ~/.ssh && chmod 700 ~/.ssh
    # Create SSH folder in the CONTAINER and give it the correct permission.

    - ssh-keyscan $DOMAIN >> ~/.ssh/known_hosts
    # Use "ssh-keyscan" to retrieve the public SSH host keys of your domain
    # and store it in the CONTAINER's "known_hosts"

  script:
    - echo "Runner is in $(pwd) as $(whoami)."
    # From this "pwd", you will see the working directory
    # is still in the CONTAINER (/builds/username/repo-name)

    - sshpass -p $JOHN_PASSWORD ssh johnsmith@$DOMAIN "pwd && whoami && echo \"SSH successfully as \$(whoami) with password authentication!\""
    # "pwd" and "whoami" in this line is executed in the remote server.

    - echo "Runner is back in $(pwd) as $(whoami)."
    # And on this command, the Runner is back to the CONTAINER.
```

![29](assets/29.jpg)


### Defining workflow ⚙️

My first `.gitlab-ci.yml`: [Link coming soon](https://gitlab.com/)

This is written based on my CI/CD need for my Vue×Node monorepo that is made up of 2 sections, frontend and backend.

**Without CI/CD**, my deployment workflow looks something like this:

1. Run a script to lint my source codes
2. Run a script to compile static files from my source codes
3. Run a script to sync/upload the compiled files to my production server
4. SSH in to the production server
5. Run a script in the production server to restart my node server

While it was simple enough to sit in front of my computer and run some scripts/commands on my local machine can get the job done, this is tedious and mundane.

**With this CI/CD**, everytime i push/merge to master, it will:

1. SSH in to remote server
2. Git pull (the changes/updates)
3. Compile from source files directly
4. Restart servers

Simple, and i'm happy with it 🥰

### Final notes

Hope this is practical enough to help you with implementing CI/CD in your workflow.

Feel free to [let me know](mailto:lucidkodo@gmail.com) if you spot something wrong in this document!

Read to Gitlab's own documentation to learn more on how to fully utilize the Runner! There are so much that can be done!

Gitlab Runner is just one of the many CI/CD tools out there with their own implementations.

Don't stop learning! 🤓

### Extra reads:

[What is containerization?](https://hackernoon.com/what-is-containerization-83ae53a709a6)

[What is docker?](https://www.simplilearn.com/tutorials/docker-tutorial/what-is-docker-container)

[What is SSH?](https://www.ssh.com/ssh/tunneling/)

[Gitlab CI/CD Doc](https://docs.gitlab.com/ee/ci/introduction/)

[Similar Tutorial](https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/)
